import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ElectronService} from "ngx-electron";

const HexRGB = require('hex-rgb');
@Component({
    selector: 'app-chart',
    templateUrl: './chart.page.html',
    styleUrls: ['./chart.page.scss'],
})
export class ChartPage implements OnInit {
    public a = "";
    public color = "";
    all = 0;
    values = [];
    percentages = [];
    stringarray = [];
    valuestosave=[];
    array = [];
    colors = [];
    rect: any;

    constructor(public activated: ActivatedRoute, private electron: ElectronService) {
    }

    ngOnInit() {

        this.a = this.activated.snapshot.paramMap.get('boxes');
        this.stringarray = this.a.split(',');

        for (let i = 0; i < this.stringarray.length; i++) {
            this.array[i] = parseInt(this.stringarray[i]);
            this.all += this.array[i];
        }
        this.electron.ipcRenderer.send('get-col', this.stringarray.length.toString());
        this.electron.ipcRenderer.on('colors', (event, arg) => {
            for(let i = 0; i < this.array.length; i++) {
                this.colors.push(arg.colors[i]['value']);

            }
            this.getangles(this.all, this.array);
            this.draw();

        });



    }
    openExpl(){
        this.valuestosave.push({total: this.all, values: this.stringarray});
        this.electron.ipcRenderer.send('open-dialog',  this.valuestosave);

    }




    getangles(total, values) {
        for (let i = 0; i < this.array.length; i++) {
            this.values[i] = (values[i] / total);
            this.percentages[i] = ((values[i] / total) * 100).toFixed(2);
        }


    }

    draw() {
        var canvas = <HTMLCanvasElement>document.getElementById("canvas");
        var textcanvas = <HTMLCanvasElement>document.getElementById("textcanvas");
        var ctx = canvas.getContext('2d');
        var ctx2 = textcanvas.getContext('2d');
        var lastend = 0;
        var color = "";
        for (var i = 0; i < this.values.length; i++) {
            ctx.beginPath();
            color =  HexRGB(this.colors[i], {format: 'array'}).toString();
            ctx.fillStyle = 'rgb(' + color + ')';
            ctx2.fillStyle ='rgb(' + color + ')';
            ctx.beginPath();
            // Arc Parameters: x, y, radius, startingAngle (radians), endingAngle (radians), antiClockwise (boolean)
            ctx.arc(canvas.width / 2, canvas.height / 2, canvas.height / 2.5, lastend, lastend + (Math.PI * 2 * (this.values[i])), false);
            ctx2.fillRect(textcanvas.width / 2 - 80, i * 50, 30, 30);
            ctx2.font = "20px Arial";
            ctx2.fillText(this.array[i] + " = " + this.percentages[i] + " %", textcanvas.width / 2, 22 + (i * 50));
            ctx.lineTo(canvas.width / 2, 200);
            ctx2.fill();
            ctx.fill();

            lastend += Math.PI * 2 * (this.values[i]);
        }
    }


}
