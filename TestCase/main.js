// Modules to control application life and create native browser window
const {app, BrowserWindow, dialog} = require('electron');
const {ipcMain} = require('electron');
var fs = require('fs');
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow() {

    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 900,
        height: 900,
        webPreferences: {
            enableRemoteModule: false,
            nodeIntegration: true,
            preload: 'preload.js'
        }
    })
    // and load the index.html of the app.
    mainWindow.loadFile('www/index.html')

    // Open the DevTools.
    // mainWindow.webContents.openDevTools()

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') app.quit()
});

app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) createWindow()
});

ipcMain.on('open-error', (event) => {
    dialog.showErrorBox('Invalid Input', "Please fill in all fields");
});

ipcMain.on('open-dialog', (event, arg) => {
    let string = 'Total: ' + arg[0]['total'] + '\n  Values: ' + arg[0]['values'] + '\n\n---------------------------------\n';
    /*let option = {defaultPath: app.getPath('documents') + '\\values.txt'};
    dialog.showSaveDialog(option, 'w', function (err, path){*/
    fs.appendFile(app.getPath('downloads') + '\\values.txt', string, (err) => {
        if (err) {
            dialog.showErrorBox("error", "An error ocurred creating the file " + err.message)
        }

        dialog.showMessageBox({
            type: 'info',
            buttons: ['Thanks!'],
            title: 'Succes!',
            message: 'The values have been saved in the "downloads" folder'
        });


    });
    /*});*/
});


ipcMain.on('get-col', (event, arg) => {
    var request = require('request');
    request('https://api.noopschallenge.com/hexbot?count=' + arg, function (error, response, body) {
        let colorarray = [];
        colorarray = JSON.parse(body);
        event.sender.send('colors', colorarray);
        console.log(colorarray['colors'][0]);
    });
});


//https://noopschallenge.com/challenges/hexbot
//https://www.npmjs.com/package/request
// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
