import {Component, inject} from '@angular/core';
import {File} from '@ionic-native/file/ngx';
import {Events, ModalController, NavController, PopoverController, ToastController} from "@ionic/angular";
import {ElectronService} from "ngx-electron";



@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {
    private insertstring;
    private blob: Blob;

    constructor(private nav: NavController, private  file: File, public toastController: ToastController, private electron: ElectronService) {
    }

    all = "";
    boxes = [];
    boxarray = [];
    total = 0;
    array = [];

    newBox() {
        this.boxarray.push(1);

    }

    saveValues() {
        for (let i = 0; i < this.boxes.length; i++) {
            this.total += this.boxes[i];

        }
        if (this.boxarray.length != this.boxes.length || this.boxarray.length == 0) {
            this.electron.ipcRenderer.send('open-error');
        } else {

            this.all = this.boxes.toString();
            this.nav.navigateForward('/chart/' + this.all);
        }

    }

    async presentToast() {
        const toast = await this.toastController.create({
            message: 'Your values have been saved!',
            duration: 2000
        });
        toast.present();
    }
}


